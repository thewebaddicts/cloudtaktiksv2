<section class="quote-section">
    <div class="quote " style="background-image:  url('{{ env('APP_URL') }}/images/SVG/bigcloude.svg')">
        <h1>{{ $title }}</h1>
        {{-- @if (isset($quote) && count($quote) > 0) --}}
        <div class="content-fluid">
            <form method="POST" action="/quoterequest">
                @csrf
                <div class="data">

                    <div class="inp">
                        <h3>full name</h3>
                        <input type="text" name="full_name" value="{{ old('full_name') }}" class=" input-field"
                            placeholder="FullName ..." required />
                        @error('full_name')
                            <span style="color: red ;font-size: 11px">{{ $message }}</span>
                        @enderror

                    </div>
                    <div class="inp">
                        <h3>phone number </h3>
                        <input type="text" name="phone_number" value="{{ old('phone_number') }}" class=" input-field"
                            placeholder="Phone Number ..." required />
                        @error('phone_number')
                            <span style="color: red ;font-size: 11px">{{ $message }}</span>
                        @enderror

                    </div>

                </div>
                <div class="data">
                    <div class="inp">
                        <h3>email</h3>
                        <input type="text" name="email" value="{{ old('email') }}" class=" input-field"
                            placeholder="Email ..." required />
                        @error('email')
                            <span style="color: red ;font-size: 11px">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="inp">
                        <h3>company name </h3>
                        <input type="text" name="company_name" value="{{ old('company_name') }}"
                            class=" input-field" placeholder="Company Name ..." required />
                        @error('company_name')
                            <span style="color: red ;font-size: 11px">{{ $message }}</span>
                        @enderror
                    </div>

                </div>
                <div class="data">
                    <div class="inp">
                        <h3>country </h3>

                        <select class="select-1" name="country" placeholder=" — Country —" required>
                            <option value="1"> — Country —</option>
                            <option value="2">Country1</option>
                            <option value="3">Country2</option>
                            <option value="4">Country3</option>
                            <option value="5">Country4</option>
                        </select>
                    </div>
                    <div class="inp">
                        <h3>title </h3>
                        <input type="text" name="title" value="{{ old('title') }}" class=" input-field"
                            placeholder="Title ..." required />
                        @error('title')
                            <span style="color: red ;font-size: 11px">{{ $message }}</span>
                        @enderror
                    </div>

                </div>
                <div class="data2">
                    <div class="inp">
                        <h3>select products </h3>
                        <select class="select-1" name="products" placeholder=" — Select —" required>
                            <option value="1">— Select —</option>
                            <option value="2">Suze Linux</option>
                            <option value="3">Citrix</option>
                            <option value="4">Cisco Meraki Firewall</option>
                            <option value="5">Office 365</option>
                            <option value="6">Anti-Virus</option>

                        </select>
                        @error('products')
                            <span style="color: red ;font-size: 11px">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="inp">
                        <h3>select services </h3>
                        <select class="select-1" name="products" placeholder=" — Select —" required>
                            <option value="1">— Select —</option>
                            <option value="2"> SAP B1</option>
                            <option value="3">Office 365 Setup</option>
                            <option value="4">Linux Suse setup and optimization</option>
                            <option value="5">Infrastructure SLA</option>

                        </select>
                        @error('products')
                            <span style="color: red ;font-size: 11px">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="inp">
                        <h3>Special requirements (Optional) </h3>
                        <textarea type="text" name="text" value="{{ old('text') }}" class=" input-field1"
                            placeholder=" Special Requirements …"></textarea>
                        @error('text')
                            <span style="color: red ;font-size: 11px">{{ $message }}</span>
                        @enderror
                    </div>


                </div>


                <div class="col">
                    <div class="input-container">
                        <div class="frm">
                            <label for="check">

                                <input type="checkbox" class="checkbox" id="check">
                                <span class="checkbox-text">I'm not a robot</span>
                            </label>
                        </div>
                        <div class="reca">
                            <picture><img src="https://www.gstatic.com/recaptcha/api2/logo_48.png"></picture>
                            <div><span class="recaptcha">reCAPTCHA</span>
                            </div>
                            <div> <a href="#">Privacy</a>
                                <span class="dash">-</span>
                                <a href="#">Terms</a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="quote-button">
                    <div class="sub">
                        <input type="submit" value="Submit">
                    </div>
                </div>
            </form>
        </div>
        {{-- @endif --}}
    </div>

</section>
