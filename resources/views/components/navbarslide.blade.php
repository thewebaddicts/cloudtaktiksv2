<nav class="navbar-slide">
    <div class="content-nav-about content-fluid">
        <picture class="logo">
            <a href="{{ env('APP_URL') }}"><img src="{{ env('APP_URL') }}/images/logo2.png" alt="">
            </a>
        </picture>

        <div class="menu-about">
            <ul>
                <ul>
                    {{-- <li><a href="{{ route('about-us') }}">About Us</a> </li> --}}
                    <li class="dropdown">

                        <a class="dropbtn">About Us</a>
                        <picture><img src="{{ env('APP_URL') }}/images/SVG/ddarrow.svg" alt=""></picture>
                        <div class="dropdown-content">
                            <a href="{{ route('about-us') }}">About-us</a>
                            <hr style="opacity:0.3">
                            <a href="{{ route('contact') }}">Contact-us</a>

                        </div>
                    </li>
                    <li><a href="{{ route('offerings') }}">Cloud Offerings</a></li>
                    {{-- <li><a href="{{ route('product1') }}">Our Products</a></li> --}}
                    <li class="dropdown">

                        <a class="dropbtn">Our Products</a>
                        <picture><img src="{{ env('APP_URL') }}/images/SVG/ddarrow.svg" alt=""></picture>
                        <div class="dropdown-content">
                            <a href="{{ route('product1') }}">Citrix</a>
                            <hr style="opacity:0.3">
                            <a href="{{ route('product2') }}">Our Products</a>

                        </div>
                    </li>
                    {{-- <li><a href="{{ route('service') }}">Our Services</a></li> --}}
                    <li class="dropdown">

                        <a class="dropbtn">Our Services</a>
                        <picture><img src="{{ env('APP_URL') }}/images/SVG/ddarrow.svg" alt=""></picture>
                        <div class="dropdown-content">
                            <a href="{{ route('service') }}">Services</a>
                            {{-- <a href="{{ route('product2') }}">Our Products</a> --}}

                        </div>
                    </li>
                    <li><a href="{{ route('customers') }}">Customers</a></li>
                    {{-- <li><a href="">Search</a>
                        <picture style="border-top: 10px"><img
                                src="{{ env('APP_URL') }}/images/SVG/Icon feather-search.svg" alt=""></picture>
                    </li> --}}
                    <a href="{{ route('quote') }}">
                        <li class="nav-button">
                            Get Started</li>
                    </a>

                </ul>
            </ul>
        </div>
    </div>
</nav>

<script language="javascript" src="/js/require.js?v={{ env('CACHE_VERSION') }}"></script>
<script language="javascript" src="/js/init.js?v={{ env('CACHE_VERSION') }}"
attr-cache-version="{{ env('CACHE_VERSION') }}"></script>
