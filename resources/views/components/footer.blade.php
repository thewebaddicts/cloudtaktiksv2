<section class="footer">
    <div class="content-fluid">
        <div class="rows">
            <div class="sitemape">
                <h1>Sitemap</h1>
                <ul>
                    <li>About-Us</li>
                    <li>Cloud Offering</li>
                    <li>Customers</li>
                    <li>Contact us</li>
                </ul>
            </div>
        </div>
        <div class="rows">
            <div class="sitemape">
                <h1>OUR PRODUCTS</h1>
                <ul>
                    <li>Suze Linux</li>
                    <li>Citrix</li>
                    <li>Cisco Meraki Firewall</li>
                    <li>Office 365</li>
                    <li>Anti-Virus</li>
                </ul>
            </div>
        </div>
        <div class="rows">
            <div class="sitemape">
                <h1>Our services</h1>
                <ul>
                    <li>SAP B1</li>
                    <li>Office 365 Setup</li>
                    <li>Linux Suse setup and optimization</li>
                    <li>Infrastructure SLA</li>
                </ul>
            </div>
        </div>
        <div class="rows">
            <div class="sitemape">
                <h1><a href="{{ route('contact') }}" style="text-decoration: none">Contact us</a> </h1>
                <ul class="location-info">
                    <li> <img src="{{ env('APP_URL') }}/images/SVG/mail.svg" alt=""> sales@cloudtaktiks.com</li>
                    <ul class="pad">
                        <li><span>.</span>
                            Evagorou, 31 Evagoras Complex, 2nd Floor, <br> Flat/Office 24 1066, Nicosia, Cyprus</li>
                        <li> <img src="{{ env('APP_URL') }}/images/SVG/phone.svg" alt="">+34 689 872 690</li>
                        <li><span>.</span>
                            Justinian St, Clemenceau Justinian Center, 9th <br> Floor Beirut, Lebanon</li>
                        <li> <img src="{{ env('APP_URL') }}/images/SVG/phone.svg" alt="">+961 81 935 241</li>
                    </ul>

                </ul>
                <div class="social">
                    <h1>Follow us</h1>
                    <picture><img src="{{ env('APP_URL') }}/images/SVG/insta.svg" alt=""></picture>
                    <picture><img src="{{ env('APP_URL') }}/images/SVG/facebook.svg" alt=""></picture>
                </div>
            </div>
        </div>

    </div>
    <div class="content-fluid">
        <div class="dev">
            <ul>
                <li>Terms&Conditions</li>
                <li style=>PrivacyPolicy</li>
                <li>CookiesPolicy</li>

            </ul>
            <div class="devv">© {{ date('Y') }} Cloud Taktiks - Web Design and Development by <a href="https://thewebaddicts.com/"> The Web Addicts</a>
            </div>
        </div>
    </div>
</section>
