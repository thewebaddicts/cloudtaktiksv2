<div class="burger">
    <i class="fas fa-bars fa-2x"></i>
    <i class="fas fa-times fa-2x"></i>
</div>

<nav class="navbar">
    <ul class="nav-links">
        <li class="nav-link"><a href="{{ route('about-us') }}">About-us</a></li>
        <li class="nav-link"><a href="{{ route('contact') }}">Contact-us</a></li>
        <li class="nav-link"><a href="{{ route('offerings') }}">Cloud Offerings</a></li>
        <li class="nav-link"><a href="{{ route('product1') }}">Citrix</a></li>
        <li class="nav-link"><a href="{{ route('product2') }}">Our Products</a></li>
        <li class="nav-link"><a href="{{ route('service') }}">Services</a></li>
        <li class="nav-link"><a href="{{ route('customers') }}">Customers</a></li>
    </ul>
</nav>
