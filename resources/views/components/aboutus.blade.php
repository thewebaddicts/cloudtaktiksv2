@if (isset($paragraphs) && count($paragraphs) > 0)
    @foreach ($paragraphs as $paragraphs)
        {{-- @dd($paragraphs) --}}
        @php $t = json_decode($paragraphs ->images) @endphp

        <section class="home-infos">
            <div class="info-1 "
                style="background-image:  url('{{ env('APP_URL') }}/images/SVG/bigcloude.svg')">
                <div class="content-fluid">
                    <div class="article-1">
                        <div class="image-groups">

                            <div class="group1">
                                @foreach ($t as $image)
                                    @if ($loop->index < 2)
                                        <picture class="img1"><img class="image1"
                                                src="{{ env('DATA_URL') }}/{{ $image->image }}">
                                        </picture>
                                        {{-- <picture><img src="{{ env('DATA_URL') }}/{{ $image->image }}"></picture> --}}
                                    @endif
                                @endforeach
                            </div>


                            <div class="group2">
                                @foreach ($t as $image)
                                    @if ($loop->index > 1)
                                        <picture><img src="{{ env('DATA_URL') }}/{{ $image->image }}"></picture>
                                        {{-- <picture><img src="{{ env('DATA_URL') }}/{{ $image->image }}"></picture> --}}
                                    @endif
                                @endforeach
                            </div>


                        </div>
                        <div class="text-1">
                            <div class="title">
                                <h1>Who We Are</h1>
                                <p class="p1">
                                    {{ $paragraphs['title'] }}
                                </p>
                            </div>
                            <div class="p2">{{ $paragraphs['paragraph'] }}
                            </div>

                            <a href="{{ $paragraphs['button_link'] }}">
                                <div class="info-button">
                                    {{ $paragraphs['button_label'] }}</div>
                            </a>


                        </div>

                    </div>
                </div>
        </section>
    @endforeach
@endif
