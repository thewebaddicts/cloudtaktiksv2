<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ env('APP_URL') }}/css/app.css">
    <title>QUOTE</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap"
        rel="stylesheet">
</head>

<body>
<div class="content-fluid">@include('components.bmenu')</div>
    @include('components.navbar')
    <div class="menu-marker"></div>
    @include('components.quote', [
        'title' => 'Request a quote',
    ])
    @include('components.footer')
<script>const burger = document.querySelector('.burger');
    const navbar = document.querySelector('.navbar');
    const navLink = document.querySelectorAll('.nav-link');

    burger.addEventListener('click', () => {
        navbar.classList.toggle('nav-open');
        burger.classList.toggle('burger-open');
        navLink.forEach(link => {
            link.classList.toggle('nav-link-open');
        })
    });</script>
</body>

</html>
