<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ env('APP_URL') }}/css/app.css">
    <title>OFFERINGS</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap"
        rel="stylesheet">
</head>

<body>
<div class="content-fluid">@include('components.bmenu')</div>
    @include('components.navbar')
    <section class="offers-header">
        <div class="content-fluid">
            <div class="offer-bigtitle">
                <span>Home / Cloud offerings</span>
                <h1>Cloud Offerings</h1>
            </div>
            @if (isset($offerings) && count($offerings) > 0)

                @foreach ($offerings as $offering)
                    {{-- @dd($offering) --}}
                    <picture>
                        <img
                            src="{{ env('DATA_URL') }}/offerings_head_image/{{ $offering->id }}.{{ $offering->extension_head_image }}">
                    </picture>
                @endforeach
            @endif
        </div>
    </section>
    <div class="menu-marker"></div>
    @if (isset($offerings) && count($offerings) > 0)
        @foreach ($offerings as $offerings)
            {{-- @dd($offerings) --}}
            @php $w = json_decode($offerings->offers) @endphp
            {{-- @dd($w) --}}
            <div class="offering-body">
                <div class="content-fluid">
                    <div class="boxes">
                        @foreach ($w as $offerings)
                            @if ($loop->index < 3)
                                <div class="box">
                                    <img src="{{ env('DATA_URL') . $offerings->svg }}">
                                    <h2>{{ $offerings->title }}</h2>
                                    <p>{!! $offerings->paragraph !!}</p>
                                </div>
                            @endif
                        @endforeach
                        {{-- <div class="box">
                            <img src="{{ env('APP_URL') }}/images/SVG/rain2.svg" alt="">
                            <h2>SAP B1 Dedicated Cloud</h2>
                            <p>Offered to some clients that have special more complex needs and need a private cloud to
                                run
                                their business. Mainly clients with over 75 Users with complex integration requirements.
                            </p>
                        </div>
                        <div class="box">
                            <img src="{{ env('APP_URL') }}/images/SVG/rain3.svg" alt="">
                            <h2>Cloud Infrastucture</h2>
                            <p>No matter what systems or applications you have , Cloudtaktiks will offer the necessary
                                cloud
                                infrastructures to support your cloud transformation.</p>
                        </div> --}}

                    </div>
                </div>
            </div>
        @endforeach
    @endif
    @if (isset($includings) && count($includings) > 0)
        @foreach ($includings as $includings)
            {{-- @dd($includings) --}}
            <div class="offering-body2">
                <div class="content-fluid">
                    @php $q = json_decode($includings->content) @endphp
                    {{-- @dd($q) --}}
                    <div class="boxes">
                        <h1>What is included</h1>

                        <div class="box">
                            @foreach ($q as $includings)
                                @if ($loop->index < 3)
                                    <div class="row">
                                        <img src="{{ env('DATA_URL') . $includings->svg }}">
                                        <h2>{{ $includings->details }}</h2>
                                    </div>
                                @endif
                            @endforeach

                            {{-- <div class="row">
                                <img src="{{ env('APP_URL') }}/images/SVG/db.svg" alt="">
                                <h2>2 Free Databases (For Multi Tenant)</h2>
                            </div>

                            <div class="row">
                                <img src="{{ env('APP_URL') }}/images/SVG/support.svg" alt="">
                                <h2>24/7/365 Days Support</h2>
                            </div> --}}
                        </div>
                        <div class="box">
                            @foreach ($q as $includings)
                                @if ($loop->index > 2)
                                    <div class="row">
                                        <img src="{{ env('DATA_URL') . $includings->svg }}">
                                        <h2>{{ $includings->details }}</h2>
                                    </div>
                                @endif
                            @endforeach
                            {{-- <div class="row">
                                <img src="{{ env('APP_URL') }}/images/SVG/ipi.svg" alt="">
                                <h2>3 Static Ips</h2>
                            </div>
                            <div class="row">
                                <img src="{{ env('APP_URL') }}/images/SVG/restore.svg" alt="">
                                <h2>Back & Restore Services</h2>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
<script language="javascript" src="/js/require.js?v={{ env('CACHE_VERSION') }}"></script>
<script language="javascript" src="/js/init.js?v={{ env('CACHE_VERSION') }}"
        attr-cache-version="{{ env('CACHE_VERSION') }}"></script>
<script>const burger = document.querySelector('.burger');
    const navbar = document.querySelector('.navbar');
    const navLink = document.querySelectorAll('.nav-link');

    burger.addEventListener('click', () => {
        navbar.classList.toggle('nav-open');
        burger.classList.toggle('burger-open');
        navLink.forEach(link => {
            link.classList.toggle('nav-link-open');
        })
    });</script>
    @include('components.footer')

</body>


</html>
