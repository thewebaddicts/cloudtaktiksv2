<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ env('APP_URL') }}/css/app.css">
    <title>ABOUT US</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap"
        rel="stylesheet">
</head>

<body>
<div class="content-fluid">@include('components.bmenu')</div>
    <div class="content-fluid">@include('components.navbarslide')</div>
    @include('components.navbar')

    <section class="aboutus-header">
        <div class="content-fluid">
            <div class="about-bigtitle">
                <span>Home / About Us</span>
                <h1>About Us</h1>
            </div>
            @if (isset($aboutus_video) && count($aboutus_video) > 0)
                @foreach ($aboutus_video as $aboutus_video)
                    {{-- @dd($aboutus_video) --}}
                    <div class="video">
                        <picture>
                            <img class="video-background" src="{{ $aboutus_video->image }}" alt="">

                        </picture>

                    </div>

                    <picture><a href="{{ $aboutus_video['button_link'] }}" target="blank"><img class="playbutton"
                                src="{{ env('APP_URL') }}/images/SVG/play.svg" alt=""></a>
                    </picture>
                @endforeach
            @endif
            <div class="menu-marker"></div>
        </div>

    </section>

    @include('components.aboutus')
    <div class="about-body2">
        <div class="content-fluid">
            @if (isset($informations) && count($informations) > 0)
                @foreach ($informations as $informations)
                    {{-- @dd($informations) --}}
                    @php $g = json_decode($informations ->informations) @endphp
                    {{-- @dd($g) --}}
                    <div class="boxes">
                        @foreach ($g as $informations)
                            @if ($loop->index < 4)
                                <div class="box">
                                    <img src="{{ env('DATA_URL') . $informations->svg }}">
                                    <h2>{{ $informations->title }}</h2>
                                    <p>{!! $informations->description !!}</p>
                                </div>
                            @endif
                        @endforeach
                        {{-- <div class="box">
                            <img src="{{ env('APP_URL') }}/images/SVG/simp.svg" alt="">
                            <h2>SIMPLICITY</h2>
                            <p>No more hassle of acquiring hardware infrastructure, maintenance services or platform
                                updates.
                                All you need to do is have access and log in.</p>
                        </div>
                        <div class="box">
                            <img src="{{ env('APP_URL') }}/images/SVG/tick.svg" alt="">
                            <h2>Security</h2>
                            <p>Cloudtaktiks has a state of the art cyber security and your Data will be protected and
                                hosted by
                                top security measures</p>
                        </div>
                        <div class="box">
                            <img src="{{ env('APP_URL') }}/images/SVG/case.svg" alt="">
                            <h2>ALLOWS YOU TO FOCUS ON THE BUSINESS</h2>
                            <p>With a faster and more efficient cloud platform and services, you can focus on your
                                actual
                                business and revenues</p>
                        </div> --}}
                    </div>
                @endforeach
            @endif
        </div>
    </div>
    @if (isset($description) && count($description) > 0)
        @foreach ($description as $description)
            {{-- @dd($description) --}}
            <div class="about-body3">
                <div class="content-fluid">
                    <div class="text-1">
                        <div class="title">
                            <h1>{{ $description['title'] }}</h1>
                            <p class="p1">
                                {{ $description['sub_title'] }}
                            </p>
                        </div>
                        <div class="p2 ">{!! $description['paragraph'] !!}
                        </div>
                    </div>
                    <picture>
                        <img src="{{ $description->image }}">
                    </picture>
                </div>
            </div>
        @endforeach
    @endif

    <section class="history">
        <div class="content-fluid">
            @if (isset($history) && count($history) > 0)
                @foreach ($history as $history)
                    {{-- @dd($history) --}}
                    @php $i = json_decode($history->details) @endphp
                    {{-- @dd($i) --}}
                    <h1> History</h1>

                    <div class="history-row">

                        @foreach ($i as $history)
                            @if ($loop->iteration % 2 == 0)
                                {{-- @if ($loop->index <= 5) --}}
                                <div class="row1">
                                    <div class="hist"><img src="{{ env('DATA_URL') . $history->image }}"
                                            alt=""></div>
                                    <span class="sp1">{!! $history->paragraph !!}</span>
                                </div>

                                {{-- @endif --}}
                            @else
                                <div class="row1">
                                    <div class="hist"><img src="{{ env('DATA_URL') . $history->image }}"
                                            alt=""></div>
                                    <span class="sp2">{!! $history->paragraph !!}</span>
                                </div>
                            @endif
                        @endforeach


                    </div>
                @endforeach
            @endif

        </div>

    </section>


    <section class="a-end">
        <div class="content-fluid">
            <div class="bigending">
                <h1>
                    Cloudtaktiks is committed to being the <br> trusted cloud partner of SAP Business <br> One users
                    globally

                </h1>
            </div>
        </div>
    </section>
    <script language="javascript" src="/js/require.js?v={{ env('CACHE_VERSION') }}"></script>
    <script language="javascript" src="/js/init.js?v={{ env('CACHE_VERSION') }}"
        attr-cache-version="{{ env('CACHE_VERSION') }}"></script>
<script>const burger = document.querySelector('.burger');
    const navbar = document.querySelector('.navbar');
    const navLink = document.querySelectorAll('.nav-link');

    burger.addEventListener('click', () => {
        navbar.classList.toggle('nav-open');
        burger.classList.toggle('burger-open');
        navLink.forEach(link => {
            link.classList.toggle('nav-link-open');
        })
    });</script>
    @include('components.footer')
</body>

</html>
