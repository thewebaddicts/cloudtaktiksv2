<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ env('APP_URL') }}/css/app.css">
    <title>CONTACT US</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap"
        rel="stylesheet">
</head>

<body>
<div class="content-fluid">@include('components.bmenu')</div>
    @include('components.navbar')

    <section class="contact-header">
        <div class="content-fluid">
            <div class="offer-bigtitle">
                <span>Home / Contact Us</span>
                <h1> Contact Us</h1>
            </div>
            @if (isset($locations) && count($locations) > 0)
                @foreach ($locations as $locations)
                    @php $test = json_decode($locations['informations']) @endphp
                    {{-- @dd($test) --}}


                    <div class="di">
                        <picture>
                            <img src="{{ env('APP_URL') }}/images/map2.png" alt="">
                        </picture>
                    </div>
                    <div class="box">
                        @foreach ($test as $test)
                            @if ($loop->index < 3)
                                <div class="accordion ">
                                    <div class="accordion-item ">
                                        <div class="rows">
                                            <div class="sitemape">
                                                <ul class="location-info">
                                                    <div class="accordion-title" id="head">
                                                        <li> <img class="pin " style="width:15px"
                                                                src="{{ env('APP_URL') }}/images/SVG/pin.svg" alt="">
                                                            <span>{{ $test->city }}</span>
                                                            <picture style="width:10px"><img class="arrow active"
                                                                    src="{{ env('APP_URL') }}/images/SVG/Icon ionic-ios-arrow-down.svg"
                                                                    alt="">
                                                            </picture>
                                                        </li>

                                                    </div>
                                                    <div
                                                        class=" @if ($loop->first) open @endif accordion-content">

                                                        <li>{!! $test->address !!}
                                                        </li>
                                                        <li> <img src=" {{ env('APP_URL') }}/images/SVG/cphone.svg"
                                                                alt="">{{ $test->number }}
                                                        </li>

                                                        <li> <img src="{{ env('APP_URL') }}/images/SVG/cmail.svg"
                                                                alt="">{{ $test->email }}
                                                        </li>

                                                    </div>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach

                    </div>
                @endforeach
            @endif
        </div>

    </section>
    <div class="menu-marker"></div>
    <section class="getintouch">
        <div class="content-fluid">
            <div class="dii">
                <div class="title">
                    <h1>We love to hear from you!
                        Feel free to get in touch</h1>
                </div>
                <form method="POST">
                    @csrf
                    <div class="form">
                        <div class="name-email">
                            <div class="name">
                                <h2>full name </h2>
                                <input type="text" name="name" value="{{ old('name') }}" class=" input-field"
                                    placeholder="Full Name..." required />
                                @error('name')
                                    <span style="color: red ;font-size: 11px">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="email">
                                <h2>email </h2>
                                <input type="text" name="email" value="{{ old('email') }}" class=" input-field"
                                    placeholder="Email ..." required />
                                @error('email')
                                    <span style="color: red ;font-size: 11px">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="text">
                            <h2>message</h2>
                            <textarea type="text" name="text" value="{{ old('text') }}" class=" input-field1"
                                placeholder=" Your inquiry …" required></textarea>
                            @error('text')
                                <span style="color: red ;font-size: 11px">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="touch-button">
                            <div class="sub">
                                <input type="submit" value="submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>



    </section>
    <script language="javascript" src="/js/require.js?v={{ env('CACHE_VERSION') }}"></script>
    <script language="javascript" src="/js/init.js?v={{ env('CACHE_VERSION') }}"
        attr-cache-version="{{ env('CACHE_VERSION') }}"></script>
<script>const burger = document.querySelector('.burger');
    const navbar = document.querySelector('.navbar');
    const navLink = document.querySelectorAll('.nav-link');

    burger.addEventListener('click', () => {
        navbar.classList.toggle('nav-open');
        burger.classList.toggle('burger-open');
        navLink.forEach(link => {
            link.classList.toggle('nav-link-open');
        })
    });</script>
    @include('components.footer')

</body>


</html>
