<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ env('APP_URL') }}/css/app.css">
    <title>HOME|PRODUCT</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap"
        rel="stylesheet">
</head>

<body>
<div class="content-fluid">@include('components.bmenu')</div>
    @include('components.navbar')
    <div class="products-body1">
        <div class="products-bigtitle">
            <span>Home / Our Products</span>
            <h1>Our Products</h1>
        </div>
        <div class="content-fluid">
            @if (isset($productproduct) && count($productproduct) > 0)
                @foreach ($productproduct as $productproduct)
                    {{-- @dd($productproduct) --}}
                    <div class="text-1">
                        <div class="title">
                            <h2>{{ $productproduct['title'] }}</h2>

                        </div>
                        <div class="p1 ">{{ $productproduct['paragraph'] }}
                        </div>
                    </div>
                    <picture>
                        <img src="{{ $productproduct->image }}">
                    </picture>
                @endforeach
            @endif
        </div>
    </div>
    <div class="products-body2">
        <div class="content-fluid">

            <div class="boxes">
                <div class="tit">
                    <h1>Sap business applications</h1>
                </div>
                <div class="box">
                    <div class="row">
                        <img src="{{ env('APP_URL') }}/images/SVG/hana.svg" alt="">
                        <h2>Sap B1 Dedicated for HANA
                        </h2>
                        <div class="divv">
                            <h3>SAP B1 Mobile</h3>
                        </div>
                        <div class="divv">
                            <h3>SAP B1 Sales</h3>
                        </div>
                        {{-- <div class="divv">
                            <h3>Intercompany</h3>
                        </div>
                        <div class="divv">
                            <h3>Integrations apps</h3>
                        </div>
                        <div class="divv">
                            <h3>Intercompany</h3>
                        </div>
                        <div class="divv">
                            <h3>Integrations apps</h3>
                        </div> --}}
                    </div>
                    <div class="row">
                        <img src="{{ env('APP_URL') }}/images/SVG/sql.svg" alt="">
                        <h2>Sap B1 SQL on Cloud</h2>
                        <div class="divv">
                            <h3>SAP B1 Mobile</h3>
                        </div>
                        {{-- <div class="divv">
                            <h3>SAP B1 Sales</h3>
                        </div>
                        <div class="divv">
                            <h3>Intercompany</h3>
                        </div>
                        <div class="divv">
                            <h3>Integrations apps</h3>
                        </div>
                        <div class="divv">
                            <h3>Intercompany</h3>
                        </div>
                        <div class="divv">
                            <h3>Integrations apps</h3>
                        </div> --}}
                    </div>
                    <div class="row">
                        <img src="{{ env('APP_URL') }}/images/SVG/hana.svg" alt="">
                        <h2>Sap B1 Dedicated for HANA<br>
                            and SQL</h2>
                        <div class="divv">
                            <h3>SAP B1 Mobile</h3>
                        </div>
                        <div class="divv">
                            <h3>SAP B1 Sales</h3>
                        </div>
                        <div class="divv">
                            <h3>Intercompany</h3>
                        </div>
                        <div class="divv">
                            <h3>Integrations apps</h3>
                        </div>
                        {{-- <div class="divv">
                            <h3>Intercompany</h3>
                        </div>
                        <div class="divv">
                            <h3>Integrations apps</h3>
                        </div> --}}
                    </div>
                    <div class="row">
                        <img src="{{ env('APP_URL') }}/images/SVG/sql.svg" alt="">
                        <h2>SAP B1 HANA ON CLOUD</h2>
                        <div class="divv">
                            <h3>SAP B1 Mobile</h3>
                        </div>
                        <div class="divv">
                            <h3>SAP B1 Sales</h3>
                        </div>
                        {{-- <div class="divv">
                            <h3>Intercompany</h3>
                        </div>
                        <div class="divv">
                            <h3>Integrations apps</h3>
                        </div>
                        <div class="divv">
                            <h3>Intercompany</h3>
                        </div>
                        <div class="divv">
                            <h3>Integrations apps</h3>
                        </div> --}}
                    </div>

                </div>
            </div>
        </div>
    </div>
    <section class="background" style="background-image:  url('{{ env('APP_URL') }}/images/product-end.png')">
    </section>
    <section class="float">
        <div class="content-fluid">
            <div class="notification">
                <div class="noti-box">
                    <div class="bigtitle">
                        READY TO MOVE YOUR SAP B1 TO CLOUD?
                    </div>
                    <div class="subtitle">Let us plan a hassle free move!</div>
                    <div class="detai">SAP HANA and SAP SQL as low as $29.99 per user per month</div>
                    <div class="try-button">
                        <div class="sub">
                            <a href="">Try it</a>
                        </div>
                    </div>


                </div>

            </div>
        </div>
    </section>



    @include('components.footer')
<script>const burger = document.querySelector('.burger');
    const navbar = document.querySelector('.navbar');
    const navLink = document.querySelectorAll('.nav-link');

    burger.addEventListener('click', () => {
        navbar.classList.toggle('nav-open');
        burger.classList.toggle('burger-open');
        navLink.forEach(link => {
            link.classList.toggle('nav-link-open');
        })
    });</script>
</body>

</html>
