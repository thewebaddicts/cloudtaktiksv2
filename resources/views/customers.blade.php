<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ env('APP_URL') }}/css/app.css">
    <title>CUSTOMERS</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap"
        rel="stylesheet">
</head>

<body>
<div class="content-fluid">@include('components.bmenu')</div>
    <div class="content-fluid">@include('components.navbarslide')</div>
    @include('components.navbar')
    <section class="offers-header">
        <div class="content-fluid">
            <div class="offer-bigtitle">
                <span>Home / Customers</span>
                <h1> Customers</h1>
            </div>

            <picture>
                <img src="{{ env('APP_URL') }}/images/custom.png" alt="">
            </picture>

        </div>
    </section>
    <div class="menu-marker"></div>
    @if (isset($customers) && count($customers) >= 0)
        @foreach ($customers as $customers)
            {{-- @dd($customers) --}}
            <section class="custom-stat">
                <div class="content-fluid">
                    <picture>
                        <img src="{{ $customers->image }}">
                    </picture>
                    <div class="text-1">
                        <div class="title">
                            <h2>{!! $customers['title'] !!}</h2>

                        </div>
                        <div class="p1">{!! $customers['paragraph'] !!}
                            <ul>
                                <li><img src="{{ env('APP_URL') }}/images/SVG/arrow.svg" alt="">300 + SAP B1 Customers
                                </li>
                                <li><img src="{{ env('APP_URL') }}/images/SVG/arrow.svg" alt="">750 + SAP B1 Das</li>
                                <li><img src="{{ env('APP_URL') }}/images/SVG/arrow.svg" alt="">3500 + SAP B1 Cloud
                                    Users</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        @endforeach
    @endif

    <section class="map">
        <div class="content-fluid">
            <div class="title">
                <h2>Clients locations</h2>

            </div>
            <picture>
                <img src="{{ env('APP_URL') }}/images/map1.png" alt="">
            </picture>
        </div>
    </section>
    @if (isset($stories) && count($stories) >= 0)
        @foreach ($stories as $stories)
            {{-- @dd($stories) --}}
            <section class="stories">
                <div class="content-fluid">
                    <div class="title">
                        <h2>Success Stories</h2>

                    </div>

                    @php $y = json_decode($stories ->content) @endphp
                    {{-- @dd($y) --}}

                    <div class="one">
                        <div class="carousel owl-carousel owl-theme" data-carousel-items="1" data-carousel-autowidth="true"
                             data-carousel-dots="false" loop="flase" data-carousel-loop="true" data-carousel-autoplay="true">
                            @foreach ($y as $stories)
                                @if ($loop->index < 5)
                                    <div class="story">
                                        <picture class="iphone"><img
                                                src="{{ env('DATA_URL') . $stories->iphone }}">
                                        </picture>

                                        <div class="box">
                                            <picture><img src="{{ env('DATA_URL') . $stories->svg }}"></picture>
                                            <p>{!! $stories->paragraph !!}</p>
                                            <div class="d">{!! $stories->details !!}</div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>


                </div>
            </section>
        @endforeach
    @endif
    <script language="javascript" src="/js/require.js?v={{ env('CACHE_VERSION') }}"></script>
    <script language="javascript" src="/js/init.js?v={{ env('CACHE_VERSION') }}"
        attr-cache-version="{{ env('CACHE_VERSION') }}"></script>
<script>const burger = document.querySelector('.burger');
    const navbar = document.querySelector('.navbar');
    const navLink = document.querySelectorAll('.nav-link');

    burger.addEventListener('click', () => {
        navbar.classList.toggle('nav-open');
        burger.classList.toggle('burger-open');
        navLink.forEach(link => {
            link.classList.toggle('nav-link-open');
        })
    });</script>

    @include('components.footer')
</body>


</html>
