<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ env('APP_URL') }}/css/app.css">
    <title>HOME|SERVICES</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap"
        rel="stylesheet">
</head>

<body>
<div class="content-fluid">@include('components.bmenu')</div>
    @include('components.navbar')
    <div class="services-body">
        <div class="services-bigtitle">
            <span>Home / Our Services</span>
            <h1>Our Services</h1>
        </div>

        <div class="menu-marker"></div>
        @if (isset($servicebyid))
            {{-- @dd($servicespage) --}}

                <div class="content-fluid services">
                    <div class="left-col">
                        <picture>
                            <img src="{{ $servicebyid->image }}">
                        </picture>
                    </div>
                    <div class="text-1">
                        <div class="title">
                            <h2>{!! $servicebyid['title'] !!}
                            </h2>

                        </div>
                        <div class="p1 scroll">{!! $servicebyid['paragraph'] !!}
                        </div>
                    </div>

                </div>
             @endif
    </div>

    <section class="switch">
        <div class="content-fluid">
            <div class="options clicked ">
                <div class="link">
                    <div class="carousel owl-carousel owl-theme" data-carousel-items="1" data-carousel-autowidth="true"
                        data-carousel-dots="false" loop="false" data-carousel-loop="true"
                        data-carousel-autoplay="false">

                        @if (isset($servicespage))
                            @foreach ($servicespage as $service)
                                <a href="{{ route('service1', ['id' => $service['id']]) }}">
                                    <div class="option @if($service['id'] == $servicebyid['id']) selected @endif" style="display:block" >
                                        {!! $service['title'] !!}
                                    </div>
                                </a>
                            @endforeach
                        @endif
                    </div>


                </div>
            </div>
        </div>

    </section>




    <script language="javascript" src="/js/require.js?v={{ env('CACHE_VERSION') }}"></script>
    <script language="javascript" src="/js/init.js?v={{ env('CACHE_VERSION') }}"
        attr-cache-version="{{ env('CACHE_VERSION') }}"></script>
    <script>const burger = document.querySelector('.burger');
    const navbar = document.querySelector('.navbar');
    const navLink = document.querySelectorAll('.nav-link');

    burger.addEventListener('click', () => {
        navbar.classList.toggle('nav-open');
        burger.classList.toggle('burger-open');
        navLink.forEach(link => {
            link.classList.toggle('nav-link-open');
        })
    });
    </script>
    @include('components.footer')
</body>

</html>
