<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ env('APP_URL') }}/css/app.css">
    <title>CITRIX|PRODUCT</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap"
        rel="stylesheet">
</head>

<body>
<div class="content-fluid">@include('components.bmenu')</div>
    @include('components.navbar')

    <div class="citrix-body1">
        <div class="citrix-bigtitle">
            <span>Home / Citrix</span>
            <h1>Our Products</h1>
        </div>
        @if (isset($citrixproduct) && count($citrixproduct) > 0)
            @foreach ($citrixproduct as $citrixproduct)
                <div class="content-fluid">
                    {{-- @dd($citrixproduct) --}}
                    <div class="text-1">
                        <div class="title">
                            <h2>{{ $citrixproduct['title'] }}</h2>

                        </div>
                        <div class="p1">{!! $citrixproduct['paragraph'] !!}
                        </div>
                    </div>
                    <picture>
                        <img src="{{ $citrixproduct->image }}">
                    </picture>
                </div>
            @endforeach
        @endif
    </div>
    <div class="menu-marker"></div>
    <div class="offering-body2">
        <div class="content-fluid">
            @if (isset($citrixcontent) && count($citrixcontent) > 0)
                @foreach ($citrixcontent as $citrixcontent)
                    <div class="boxes">
                        {{-- @dd($citrixcontent) --}}
                        @php $r = json_decode($citrixcontent ->content) @endphp
                        {{-- @dd($r) --}}
                        <div class="box">
                            @foreach ($r as $citrixcontent)
                                @if ($loop->index < 3)
                                    <div class="row">
                                        <img src="{{ env('DATA_URL') . $citrixcontent->svg }}">
                                        <h2>{{ $citrixcontent->details }}</h2>
                                    </div>
                                @endif
                            @endforeach
                            {{-- <div class="row">
                                <img src="{{ env('APP_URL') }}/images/SVG/devices.svg" alt="">
                                <h2>Connect From Any Device</h2>
                            </div>
                            <div class="row">
                                <img src="{{ env('APP_URL') }}/images/SVG/web.svg" alt="">
                                <h2>Web Enable Any Application</h2>
                            </div> --}}
                        </div>
                        <div class="box">
                            @foreach ($r as $citrixcontent)
                                @if ($loop->index > 2)
                                    <div class="row">
                                        <img src="{{ env('DATA_URL') . $citrixcontent->svg }}">
                                        <h2>{{ $citrixcontent->details }}</h2>
                                    </div>
                                @endif
                            @endforeach
                            {{-- <div class="row">
                                <img src="{{ env('APP_URL') }}/images/SVG/webdd.svg" alt="">
                                <h2>Simple To Use</h2>
                            </div>
                            <div class="row">
                                <img src="{{ env('APP_URL') }}/images/SVG/webddd.svg" alt="">
                                <h2>Scalable</h2>
                            </div> --}}
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
<script language="javascript" src="/js/require.js?v={{ env('CACHE_VERSION') }}"></script>
<script language="javascript" src="/js/init.js?v={{ env('CACHE_VERSION') }}"
        attr-cache-version="{{ env('CACHE_VERSION') }}"></script>
<script>const burger = document.querySelector('.burger');
    const navbar = document.querySelector('.navbar');
    const navLink = document.querySelectorAll('.nav-link');

    burger.addEventListener('click', () => {
        navbar.classList.toggle('nav-open');
        burger.classList.toggle('burger-open');
        navLink.forEach(link => {
            link.classList.toggle('nav-link-open');
        })
    });</script>

    @include('components.footer')
</body>

</html>
