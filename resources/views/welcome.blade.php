<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ env('APP_URL') }}/css/app.css">
    <title>CLOUD TAKTIKS</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&display=swap"
        rel="stylesheet">



</head>

<body>
<div class="content-fluid">@include('components.bmenu')</div>
    <div class="content-fluid">@include('components.navbarslide')</div>
    @include('components.mainnav')
    @if (isset($slideshow) && count($slideshow) > 0)
        <div class="carousel owl-carousel owl-theme" data-carousel-items="1" data-carousel-width="true"
            data-carousel-dots="true" loop="true" data-carousel-loop="true" data-carousel-autoplay="true">
            @foreach ($slideshow as $slide)
                <section class=" head ">
                    <header class="top" style="background-image:  url('{{ $slide->image }}')">

                        <div class="content-head content-fluid ">
                            <div class="title">{{ $slide['label'] }}
                            </div>
                            <div class="sub-title">
                                {!! $slide['description'] !!}
                            </div>
                            <a href="{{ $slide['button_link'] }}">
                                <div class="head-button">
                                    {{ $slide['button_text'] }}</div>
                            </a>


                        </div>
                    </header>
                </section>
            @endforeach
        </div>
    @endif

    <div class="menu-marker"></div>

    @include('components.aboutus')
    @if (isset($banner) && count($banner) > 0)
        @foreach ($banner as $banner)
            @if (count(json_decode($banner['text'])) > 0)
                <section class="home-body1" style="background-image:  url('{{ $banner->image }}')">


                    @foreach (json_decode($banner['text']) as $field)
                        @if ($loop->index < 2)
                            <div class="stat">
                                <div class="ligne{{ $loop->index == 1 ? '2' : '' }}"></div>
                                <h2>{!! $field->text_number !!}</h2>
                                <div class="inf">
                                    <div class="title">{!! $field->text_paragraph !!}</div>
                                    <div class="subtitle">{!! $field->text_description !!}
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </section>
            @endif
        @endforeach
    @endif
    @if (isset($products) && count($products) > 0)
        @foreach ($products as $products)
            @php $products_json = json_decode($products->prodcut) @endphp



            <div class="home-body2">
                <div class="content-fluid">
                    <h1>Our products</h1>

                    <div class="boxes">
                        @foreach ($products_json as $product)
                            {{-- @dd($product) --}}
                            @if ($loop->index < 5)
                                <div class="box">
                                    <div class="circle">
                                        <img class="svg" src="{{ env('DATA_URL') . $product->svg }}"
                                            alt="">
                                    </div>

                                    <h2>{{ $product->title }}</h2>
                                    <p>{{ $product->description }}
                                    </p>

                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    @endif
    <div class="home-body3">
        <div class="content-fluid">
            <h1>Our services</h1>
            <div class="all-services clickable">
                <div class="carousel owl-carousel owl-theme" data-carousel-items="1" data-carousel-autowidth="true"
                    data-carousel-dots="false" loop="flase" data-carousel-loop="true" data-carousel-autoplay="false">
                    @if (isset($servicespage))
                        @foreach ($servicespage as $services)
                            <div href="javascript:;"
                                class="  @if ($loop->first) expanded @endif servicess ">
                                <div class="image"> <img src="{{ $services->image }}" alt="">
                                </div>
                                <div class="informations ">
                                    <div class="title">{!! $services['title'] !!}
                                    </div>
                                    <div class="text">{!! $services['paragraph'] !!}</div>
                                    <a href="{{ route('service1', ['id' => $services['id']]) }}">
                                        <button class="services_button"> {{ $services['button_text'] }}</button></a>
                                </div>


                            </div>
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </div>
    <div class="divvvvv" style="width:0%"></div>

    @if (isset($clients) && count($clients) > 0)
        @foreach ($clients as $clients)
            {{-- @dd($r) --}}
            <section class="clients">

                <div class="content-fluid">
                    <h1>{{ $clients['label'] }}</h1>

                    <div class="client-row1">
                        @php $r = json_decode($clients->image) @endphp
                        {{-- @dd($r) --}}
                        @foreach ($r as $clients)
                            @if ($loop->index < 7)
                                <picture>
                                    <img src="{{ env('DATA_URL') . $clients->logo }}">
                                </picture>
                            @endif
                        @endforeach



                    </div>

                    <div class="client-row1">
                        {{-- @php $r = json_decode($clients->image) @endphp --}}
                        @foreach ($r as $clients)
                            @if ($loop->index >= 7)
                                <picture>
                                    <img src="{{ env('DATA_URL') . $clients->logo }}">
                                </picture>
                            @endif
                        @endforeach

                    </div>
                </div>
            </section>
        @endforeach
    @endif
    @if (isset($process) && count($process) > 0)
        @foreach ($process as $process)
            <section class="process"
                style="background-image:  url('{{ env('APP_URL') }}/images/leftcloud.png')">
                <div class="content-fluid">
                    {{-- @dd($process) --}}
                    <div class="title">
                        <h1>{{ $process['big_title'] }}</h1>
                        <div class="subtitle">
                            {{ $process->sub_title }}
                        </div>
                        <p>{!! $process->description !!}</p>
                    </div>
                    <div class="numbers">
                        <div class="numb">01</div>
                        <div class="numb">02</div>
                        <div class="numb">03</div>
                        <div class="numb">04</div>
                        <div class="numb">05</div>
                    </div>
                    <div class="dets">
                        @php $k = json_decode($process->image) @endphp
                        @foreach ($k as $process)
                            {{-- @dd($k) --}}
                            <picture>
                                <img src="{{ env('DATA_URL') . $process->svg }}">
                            </picture>
                        @endforeach

                    </div>
                    <div class="detai">
                        @foreach ($k as $process)
                            {{-- @dd($k) --}}
                            <div class="i1">
                                <h2>{{ $process->title }}</h2>
                                <p>{{ $process->description }}</p>
                            </div>
                        @endforeach


                    </div>

                </div>
            </section>
        @endforeach
    @endif
    <script language="javascript" src="/js/require.js?v={{ env('CACHE_VERSION') }}"></script>
    <script language="javascript" src="/js/init.js?v={{ env('CACHE_VERSION') }}"
        attr-cache-version="{{ env('CACHE_VERSION') }}"></script>
<script>const burger = document.querySelector('.burger');
    const navbar = document.querySelector('.navbar');
    const navLink = document.querySelectorAll('.nav-link');

    burger.addEventListener('click', () => {
        navbar.classList.toggle('nav-open');
        burger.classList.toggle('burger-open');
        navLink.forEach(link => {
            link.classList.toggle('nav-link-open');
        })
    });</script>
    {{-- <x-quote /> --}}
    @include('components.quote', [
        'title' => 'Request a quote',
    ])
    @include('components.footer')

    <script language="javascript">
        function FooterFunctions() {
            animateServices();
        }
    </script>
</body>


</html>
