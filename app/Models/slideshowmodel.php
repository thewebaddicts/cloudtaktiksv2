<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class slideshowmodel extends Model
{
    use HasFactory;
    protected $table='slideshow';

    public function getImageAttribute(){
        return env('DATA_URL')."/slideshow/".$this->id.".".$this->extension_image."?v=.".$this->version;
    }
}
