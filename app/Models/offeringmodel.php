<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class offeringmodel extends Model
{
    use HasFactory;
    protected $table='offerings';
    // public function getImageAttribute(){
    //     return env('DATA_URL')."/offerings_head_image/".$this->id.".".$this->extension_head_image."?v=.".$this->version;
    // }
}
