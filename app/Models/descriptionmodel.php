<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class descriptionmodel extends Model
{
    use HasFactory;
    protected $table='description';
    public function getImageAttribute(){
        return env('DATA_URL')."/description_images/".$this->id.".".$this->extension_image."?v=.".$this->version;
    }
}
