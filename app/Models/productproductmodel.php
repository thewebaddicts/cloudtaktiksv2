<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class productproductmodel extends Model
{
    use HasFactory;
    protected $table='product_product';
    public function getImageAttribute(){
        return env('DATA_URL')."/product_images/".$this->id.".".$this->extension_image."?v=.".$this->version;
    }
}
