<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class customerspagemodel extends Model
{
    
    use HasFactory;
    protected $table='customers';
    public function getImageAttribute(){
        return env('DATA_URL')."/diagram_image/".$this->id.".".$this->extension_diagram_image."?v=.".$this->version;
    }
}
