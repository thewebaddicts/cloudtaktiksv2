<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class citrixcontentmodel extends Model
{
    use HasFactory;
    protected $table='citrix_content';
}
