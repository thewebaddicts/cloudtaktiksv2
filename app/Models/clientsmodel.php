<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class clientsmodel extends Model
{
    use HasFactory;
    protected $table='clients';
    // public function getImageAttribute(){
    //     return env('DATA_URL')."/jsonuploads/".$this->id.".".$this->extension_image."?v=.".$this->version;
    // }
}
