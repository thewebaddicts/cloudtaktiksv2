<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class informationsmodel extends Model
{
    use HasFactory;
    protected $table='informations';
}
