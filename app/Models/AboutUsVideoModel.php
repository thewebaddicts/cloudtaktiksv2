<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AboutUsVideoModel extends Model
{
    use HasFactory;
    protected $table='aboutus_video';
    public function getImageAttribute(){
        return env('DATA_URL')."/aboutus_video_background/".$this->id.".".$this->extension_image."?v=.".$this->version;
    }
}
