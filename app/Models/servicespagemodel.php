<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class servicespagemodel extends Model
{
    use HasFactory;
    protected $table='services_page';
    public function getImageAttribute(){
        return env('DATA_URL')."/services_image/".$this->id.".".$this->extension_image."?v=.".$this->version;
    }
}
