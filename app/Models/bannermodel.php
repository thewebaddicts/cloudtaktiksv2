<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class bannermodel extends Model
{
    use HasFactory;
    protected $table='banner';

    public function getImageAttribute(){
        return env('DATA_URL')."/banner_background/".$this->id.".".$this->extension_background_image."?v=.".$this->version;
    }

}
