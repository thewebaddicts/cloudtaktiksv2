<?php

namespace App\Http\Controllers;
use App\Models\servicespagemodel;
use App\Models\ServicesOptionsModel;
use Illuminate\Http\Request;

class servicesController extends Controller
{
    public static function fifth()
    {
        $servicebyid= servicespagemodel::where('cancelled', 0)->first();
$servicespage= servicespagemodel::where('cancelled', 0)->get();
$servicesoptions= ServicesOptionsModel::where('cancelled', 0)->get();
// $description= descriptionmodel::where('cancelled', 0)->get();
return view('/services',['servicespage'=>$servicespage,'servicesoptions'=>$servicesoptions,'servicebyid'=>$servicebyid]);
 }
 public static function display_services($id){

    $servicebyid= servicespagemodel::where('cancelled', 0)->where('id',$id)->first();
    $servicespage= servicespagemodel::where('cancelled', 0)->get();
//    dd($servicebyid);

return view('/services',[ 'servicebyid'=>$servicebyid,'servicespage'=>$servicespage]);
 }
}
