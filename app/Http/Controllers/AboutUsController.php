<?php

namespace App\Http\Controllers;
use App\Models\AboutUsVideoModel;
use App\Models\paragraphsmodel;
use App\Models\informationsmodel;
use App\Models\descriptionmodel;
use App\Models\HistoryModel;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{
    public static function first()
    {

$paragraphs= paragraphsmodel::where('cancelled', 0)->get();
$informations= informationsmodel::where('cancelled', 0)->get();
$description= descriptionmodel::where('cancelled', 0)->get();
$history= HistoryModel  ::where('cancelled', 0)->get();
$aboutus_video= AboutUsVideoModel ::where('cancelled', 0)->get();
return view('about',['aboutus_video'=>$aboutus_video,'paragraphs'=>$paragraphs,'informations'=>$informations,'description'=>$description,'history'=>$history]);
 }
}
