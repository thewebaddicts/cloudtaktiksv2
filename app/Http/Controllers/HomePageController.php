<?php

namespace App\Http\Controllers;
use App\Models\slideshowmodel;
use App\Models\paragraphsmodel;
use App\Models\bannermodel;
use App\Models\servicespagemodel;
use App\Models\productsmodel;
use App\Models\servicesmodel;
use App\Models\clientsmodel;
use App\Models\processmodel;

use Illuminate\Http\Request;


class HomePageController extends Controller
{
    public static function render()
    {
        // $servicebyid= servicespagemodel::where('cancelled', 0)->first();
$slideshow = slideshowmodel::where('cancelled', 0)->get();
$paragraphs= paragraphsmodel::where('cancelled', 0)->get();
$banner = bannermodel::where('cancelled', 0)->get();
$products = productsmodel::where('cancelled', 0)->get();
//$services =servicesmodel::where('cancelled', 0)->get();
$clients =clientsmodel::where('cancelled', 0)->get();
$process =processmodel::where('cancelled', 0)->get();
        $servicespage= servicespagemodel::where('cancelled', 0)->get();



return view('welcome',['slideshow'=>$slideshow,'paragraphs'=>$paragraphs,'banner'=>$banner,'products'=>$products,'clients'=>$clients,'process'=>$process,'servicespage'=>$servicespage]);

    }
    public static function display_services($id){
        $servicebyid= servicespagemodel::where('cancelled', 0)->where('id',$id)->first();
        $servicespage= servicespagemodel::where('cancelled', 0)->get();
    //    dd($servicebyid);

    return view('/services',['servicebyid'=>$servicebyid,'servicespage'=>$servicespage]);
     }
}
// ,'servicebyid'=>$servicebyid,'servicespage'=>$servicespage 'services'=>$services,
