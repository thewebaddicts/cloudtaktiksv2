<?php

namespace App\Http\Controllers;
use App\Models\quotemodel;
use Illuminate\Http\Request;

class quoterequestController extends Controller
{
    public function request(){
       
        request()->validate([
          'full_name' => 'required',
          'phone_number' => 'required',
          'email' => 'required|email',
          'company_name' => 'required',
          'title' => 'required',

          'text' => 'required'
        ]);

      $form = new quotemodel;
      $form->full_name = request()->input('full_name');
      $form->phone_number = request()->input('phone_number');
      $form->email = request()->input('email');
      $form->company_name = request()->input('company_name');
      $form->title = request()->input('title');
      $form->requirements = request()->input('text');
      $form->save();
   
      
      return redirect()->back();
    //   ->back()->with('notification', 2)
  
      }
}
