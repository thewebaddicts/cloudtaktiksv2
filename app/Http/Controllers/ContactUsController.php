<?php

namespace App\Http\Controllers;
use App\Models\ContactUsModel;

use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function contact(){
       
        request()->validate([
          'name' => 'required',
          'email' => 'required|email',
          'text' => 'required'
        ]);

      $form = new ContactUsModel;
      $form->name = request()->input('name');
      $form->email = request()->input('email');
      $form->text = request()->input('text');
      $form->save();
   
      
      return redirect()->back();
  
      }
      
}
