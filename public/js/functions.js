// function ShowLoader() {
//     $("body").append(
//         '<div class="common-loader-initial common-loader-noanim"></div>'
//     );
// }
// function HideLoader() {
//     $(".common-loader-initial").remove();
// }

// function ShowMessage(Title, Text) {
//     $.fancybox.open(
//         '<div class="message"><h2>' + Title + "</h2><p>" + Text + "</p></div>"
//     );
// }

// function ShowIframe(link, maxwidth) {
//     $.fancybox.open({
//         src: link,
//         type: "iframe",
//         opts: {
//             iframe: {
//                 css: {
//                     width: maxwidth,
//                 },
//             },
//         },
//     });
// }
// function regex($url,$matches){
//     preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $matches);

// }

// function removeQueryNotificationString() {
//     urlObject = new URL(window.location.href);
//     urlObject.searchParams.delete("notification");
//     urlObject.searchParams.delete("notification_message");
//     urlObject.searchParams.delete("notification_title");

//     url = urlObject.href;

//     history.pushState(null, null, url);
// }

// function closeFancyBox() {
//     parent.jQuery.fancybox.getInstance().close();
// }

// var isMobile = false; //initiate as false
// // device detection
// if (
//     /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
//         navigator.userAgent
//     ) ||
//     /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
//         navigator.userAgent.substr(0, 4)
//     )
// ) {
//     isMobile = true;
// }

// function doParallax() {
//     $(".parallax, .parallaxText").css({
//         transition: "0s !important",
//         "transition-delay": "0s !important",
//         "transition-timing-function": "linear",
//     });
//     if (isMobile != true) {
//         $(".parallax").each(function () {
//             var offset = $(this).offset();
//             var positionY = (window.pageYOffset - offset.top) / 2;
//             if (positionY < 0) {
//                 positionY = 0;
//             }
//             if (positionY < 100) {
//                 $(this)
//                     .find(".parallaxText")
//                     .css("opacity", "" + (100 - positionY) / 100);
//             }
//             $(this).css("background-position", "center +" + positionY + "px");
//         });
//     }
// }
// function search_animal() {
//     // alert('hey');
//     let input = document.getElementById("search").value;
//     input = input.toLowerCase();
//     let x = document.getElementsByClassName("animals");

//     for (i = 0; i < x.length; i++) {
//         if (!x[i].innerHTML.toLowerCase().includes(input)) {
//             x[i].style.display = "none";
//         } else {
//             x[i].style.display = "list-item";
//         }
//     }
// }
// function reveal() {
//     var reveals = document.querySelectorAll(".reveal");

//     for (var i = 0; i < reveals.length; i++) {
//         var windowHeight = window.innerHeight;
//         var elementTop = reveals[i].getBoundingClientRect().top;
//         var elementVisible = 150;

//         if (elementTop < windowHeight - elementVisible) {
//             reveals[i].classList.add("active");
//         } else {
//             reveals[i].classList.remove("active");
//         }
//     }
// }

// window.addEventListener("scroll", reveal);

// function scrollup() {
//     document.querySelectorAll('a[href^="#"]').forEach((anchor) => {
//         anchor.addEventListener("click", function (e) {
//             e.preventDefault();

//             document.querySelector(this.getAttribute("href")).scrollIntoView({
//                 behavior: "smooth",
//             });
//         });
//     });
// }
// function play() {
//     $(".lists").scrollTop($("ul li:nth-child(7)").position().top);
// }
// function theli() {
//     $(document).ready(function () {
//         $(".liveli").click(function (e) {
//             $(".liveli").removeClass("active");

//             $(this).addClass("active");

//             e.preventDefault();
//             $("iframe").css("display", "block");
//             $(".w-v").css("display", "none");

//             $("#someFrame").attr("src", $(this).attr("data-link"));
//             var text = $(this).text();
//             $("#wen").text(text);
//         });
//     });
// }

const p = document.getElementById("p");
const m = document.getElementById("m");

function search() {
    let input = document.getElementById("search").value;
    if (input !== "") {
        let regExp = new RegExp(input, "gi");
        p.innerHTML = p.textContent.replace(regExp, "<mark>$&</mark>");
    }
}

function sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

// $( "li.item-ii" ).find( "li" ).css( "background-color", "red" );

//     $('#search').keyup(function(){

//         var page = $('#all_text');
//         var pageText = page.text().replace("<span>","").replace("</span>");
//         var searchedText = $('#searchfor').val();
//         var theRegEx = new RegExp("("+searchedText+")", "igm");
//         var newHtml = pageText.replace(theRegEx ,"<span>$1</span>");
//         page.html(newHtml);
//    });

// jQuery(document).ready(function ($) {
//     $('.form-search').on('submit',function(){return false;});
//     $('.form-search .btn').on('click', function(e){
//         var query = $.trim($(this).prevAll('.search-query').val()).toLowerCase();
//         $('#ab_1 .title').each(function(){
//              var $this = $(this);
//              if($this.text().toLowerCase().indexOf(query) === -1)
//                  $this.closest('#ab_1').fadeOut();
//             else $this.closest('#ab_1').fadeIn();
//         });
//     });
// )}

// function hw() {
//     $(document).ready(function () {
//         var open = true;
//         $("#exp1").click(function () {
//             $("#expp").css("height", function (i, oldSrc) {
//                 return oldSrc == "334px" ? "400px" : "334px";
//             });

//             if (open == true) {
//                 expp.css === "334px";

//                 open = false;
//             } else {
//                 expp.css === "400px";

//                 open = true;
//             }
//         });
//     });
// $(document).ready(function(event){
//     $('#exp1').on("click",function(event){
//         $('#expp').css("height","400px")
//     })
// })
// }
// function hw2() {
//     $(document).ready(function () {
//         var open = true;
//         $("#expand").click(function () {
//             $("#livetv").css("height", function (i, oldSrc) {
//                 return oldSrc == "404px" ? "500px" : "404px";
//             });

//             if (open == true) {
//                 expp.css === "404px";

//                 open = false;
//             } else {
//                 expp.css === "500px";

//                 open = true;
//             }
//         });
//     });
//     $(document).ready(function(event){
//         $('#exp1').on("click",function(event){
//             $('#expp').css("height","400px")
//         })
//     })
// }

// function search1() {
//     $(document).ready(function () {
//         $(".search").on("click", function (event) {
//             // alert('hi')

//             // $('#slides').css("-webkit-filter", " blur(8px)");
//             // $("#slides").css("animation", "shrink 0s");
//             $("#times").css("display", "flex");
//             $("#slides").css("animation", "zoominoutsinglefeatured 0s");
//             $("#search").css("display", "block");
//             $(this).css("display", "none");
//             $(".main-nav ul").css("display", "none");
//             $(".main-nav .logodiv").css("display", "none");
//             $(".owl-dots").css("display", "none");
//             $("#Mi").css("display", "none");
//             $(".close").css("display", "block");
//             $(".slide .middleimage").css("display", "none");
//             $("#searchBtn").css("display", "block");
//         });
//     });
// }
// function times() {
//     $(document).ready(function () {
//         $("#times").on("click", function (event) {
//             // alert('hi')

//             //$('#slides').css("-webkit-filter", " blur(0px)");
//             // $("#slides").css("animation", "shrink 0s");
//             $(this).css("display", "none");
//             $("#slides").css("animation", "zoominoutsinglefeatured 10s");
//             $("#search").css("display", "none");
//             $(".search").css("display", "flex");
//             $(".main-nav ul").css("display", "flex");
//             $(".main-nav .logodiv").css("display", "block");
//             $(".owl-dots").css("display", "block");
//             $(".slide .middleimage").css("display", "block");
//             $("#searchBtn").css("display", "none");

//             // $('#Mi').css("display", "none");
//             // $('.close').css("display", "block");
//         });
//     });
// }
// function fadein() {
//     $(document).ready(function() {
//         $(".slide").delay(5000).queue(function(){
//             $(this).css({"background-image":"url('/images/Tink.jpg')"});
//         });
//     });
//     // $(".slide").css("background-image", "url('/images/Tink.jpg')");

// }
// function clicked() {
//     $(".options.clicked .option").click(function () {
//         $(this)
//             .parents(".options")
//             .find(".activated")
//             .removeClass("activated");
//         $(this).toggleClass("activated");
//     });
// }

function car() {
    $(document).ready(function () {
        $("#home").owlCarousel({
            navigation: false,
            slideSpeed: 500,
            paginationSpeed: 800,
            rewindSpeed: 1000,
            singleItem: true,
            autoPlay: true,
            stopOnHover: true,
        });
    });
}
function InitializeMenuScroll() {
    // alert("dfs");
    var scroll_start = 0;
    var startchange = $(".menu-marker");
    var offset = startchange.offset();
    //  var slideshowOffset = $("#slideshow").offset();
    if (startchange.length >= 0 && $(document).scrollTop() - offset.top >= 0) {
        // $(".about-main").addClass("compact2");

        $(".navbar-slide ").addClass("compact2");

        // $(".about-main").css("top", "70px");
        // $(".togglesearch").addClass("compact");
    } else {
        // $(".about-main,.main").removeClass("compact2");
        $(".navbar-slide").removeClass("compact2");
        // $(".menu-about").css("top", "100px");
        // $(".togglesearch").removeClass("compact");
    }
}
// function InitializeMenuScroll() {
//     var scroll_start = 100;
//     var startchange = $(".menu-marker");
//     var offset = startchange.offset();

//     if (
//         startchange.length > 0 &&
//         $(document).scrollTop() - offset.top > scroll_start
//     ) {
//         $("nav.about-main,header").addClass("compact");
//     } else {
//         $("nav.about-main,header").removeClass("compact");
//     }
// }

function InitializeMenuMobile() {
    $("nav.mobile-nav .burger").click(function () {
        $(this).toggleClass("clicked");
        $(this).parent().find("ul").slideToggle(200);
    });
    // alert(" hi im here ");
}
function searchforul() {
    $("#filter").keyup(function () {
        if (this.value.length) {
            var that = this;
            $(".lists li")
                .hide()
                .filter(function () {
                    return (
                        $(this)
                            .html()
                            .toLowerCase()
                            .indexOf(that.value.toLowerCase()) !== -1
                    );
                })
                .show();
            $(".lists").show();
        } else {
            $(".lists").hide();
        }
    });
}
function searchforul2() {
    $("#search").on("submit", "form.remember", function () {
        alert("hey");
    });
}
// function colorsChange(){
//     var colors = ['#009c61', '#cc0099', '#cc9900', '#cc0033', '#0099cc', '#6600cc', '#66cc00'];
//     $("#colored-div").hover(function() {
//         $(this).css("background-color", colors[(Math.random() * colors.length) | 0])
//     }, function() {
//         $(this).css("background-color", "")
//     });
// }
// var counter = 0;
// var colors = ["#00ff00", "#ff0000", "#000000"];

// var $div = $("#colored-div");
// var interval;
// $("#colored-div")
//     .mouseenter(function () {
//         interval = window.setInterval(changeColor, 1000); //set the interval of 1 sec for image to change while hovered.
//     })
//     .mouseleave(function () {
//         window.clearInterval(interval); //clear the interval on mouseOut.
//     });

// function changeColor() {
//     var color = colors.shift();
//     colors.push(color);
//     $div.css({
//         "background-color": color,
//     });
// }
// function parallax() {
//     $(window).scroll(function () {
//         var wScroll = $(this).scrollTop();

//         $(".about_us").css({
//             transform: "translateY(-" + wScroll / 20 + "%)",
//         });
//     });
// }
// function parallax2() {
//     $(window).scroll(function () {
//         var wScroll = $(this).scrollTop();

//         $(".projects-title").css({
//             transform: "translateY(-" + wScroll / 20 + "%)",
//         });
//     });
// }
function openAccordion(elem) {
    elem.toggleClass("accordion-active");

    var elemMarginBtm = elem.css("margin-bottom");
    console.log(elemMarginBtm);

    var panel = elem.next();
    if (panel.css("max-height") != "0px") {
        panel.css("max-height", "0px");
    } else {
        panel.css("max-height", panel.prop("scrollHeight"));
    }
}
// function minus() {
//     var targetOffset = $("#Radio").offset().top;

//     var $w = $(window).scroll(function () {
//         if ($w.scrollTop() < targetOffset) {
//             $("#left").css({ position: "fixed" });
//         } else {
//             $("#left").css({ position: "absolute" });
//         }
//     });
// }
// function thescrollingbutton() {
//     $("#prev").click(function () {
//         $("html, body").animate({ scrollTop: 0 }, "slow");
//         return false;
//     });
// }
// function thescrollingli() {
//     $("#row8").click(function () {
//         $("#row8").animate({ scrollup: 0 });
//     });
// }

// function plustominus() {
//     $(document).ready(function () {
//         var open = true;
//         $(".plus").click(function () {
//             $(this).attr("src", function (i, oldSrc) {
//                 return oldSrc == "/images/plus.png"
//                     ? "/images/minus.png"
//                     : "/images/plus.png";
//             });

//             if (open == true) {
//                 plus.src === "/images/plus.png";

//                 open = false;
//             } else {
//                 plus.src === "/images/minus.png";

//                 open = true;
//             }
//         });
//     });
// }
// function minustoplus() {
// $('select').click(function(){
//     $(this).toggleClass('after');
//   });
// $(document).ready(function () {
//     var open = true;
//     $('.plus').click(function () {
//         $(this).attr('src', function (i, oldSrc) {
//             return oldSrc == '/images/plus.png' ? '/images/minus.png' : '/images/plus.png';
//         });
//         if (open == true) {
//             plus.src === '/images/plus.png'
//             open = false;
//         }
//         else {
//             plus.src === '/images/minus.png'
//             open = true;
//         }
//     });
// });
// var wasClicked = false;
// $(document).ready(function() {
//     $("select").click(function() {
//         if (!wasClicked) {
//             wasClicked = true;
//             return;
//         }
//         if ($(this).css("background") == "url('/images/plus.png') no-repeat right") {
//             $(this).css("background", "url('/images/minus.png') no-repeat right");
//         } else {
//             $(this).css("background", "url('/images/plus.png') no-repeat right");
//         }
//     });
// });
// $("select").click(function(event){
//     jQuery.fx.off = true;
//     //$("slect").toggle("");
// if( $(this).css("background", "url('/images/plus.png') no-repeat right")){
//     $(this).css("background" , "url('/images/minus.png') no-repeat right");
// }
// else if($(this).css("background", "url('/images/minus.png') no-repeat right")){
//     $(this).css("background" , "url('/images/plus.png') no-repeat right");
// }
// else{
//     $(this).css("background", "url('/images/plus.png') no-repeat right")
// }
//     event.stopPropagation();
// });
// $(document).ready(function() {
//     $("select").click(function() {
//       $("select").toggleClass("active");
//     });
//   });
// }

function AccordionInitiate() {
    $(".accordion .accordion-item:not(.active) .accordion-content").addClass(
        "initialized"
    );
    $(".accordion .accordion-item:not(.active) .accordion-content").slideUp();
    $(".accordion .accordion-title").click(function () {
        $(".accordion-content").not(".open").removeClass("open");
        if ($(".accordion-content").hasClass("open")) {
            // alert("dsfdsf");
            $(this).next(".accordion-content").removeClass("open");
        } else {
            $(this).next(".accordion-content").addClass("open");
        }
        $(".accordion-content").removeClass("open");
        $(this).next(".accordion-content").toggleClass("open");
        $(".arrow").addClass("active");
        $(this).find(".arrow").toggleClass("active");
        // $(this)
        //     .parents(".accordion")
        //     .find(".accordion-item.active")
        //     .not($(this).parents(".accordion-item"))
        //     .removeClass("active");
        // $(this).parents(".accordion-item").toggleClass("active");
        // $(this)
        //     .parents(".accordion-item")
        //     .find(".accordion-content")
        //     .slideToggle(00);

        // // try {
        // //     bLazy.revalidate();
        // // }catch (e) {

        // // }
        // $(this)
        //     .parents(".accordion")
        //     .find(".accordion-item:not(.active)")
        //     .find(".accordion-content")
        //     .slideUp(000);
    });
}
// function AccordionInitiatee() {
//     $(".accordion1 .accordion-item1:not(.active) .accordion-content1").addClass(
//         "initialized"
//     );
//     $(
//         ".accordion1 .accordion-item1:not(.active) .accordion-content1"
//     ).slideUp();
//     $(".accordion1 .accordion-title1").click(function () {
//         $(".accordion-content1").not(".open").removeClass("open");
//         if ($(".accordion-content1").hasClass("open")) {
//             // alert("dsfdsf");
//             $(this).next(".accordion-content1").removeClass("open");
//         } else {
//             $(this).next(".accordion-content1").addClass("open");
//         }
//         // $(".accordion-content").removeClass("open");
//         // $(this).next(".accordion-content").toggleClass("open");
//         // $(".arrow").addClass("active");
//         // $(this).find(".arrow").toggleClass("active");
//         $(this)
//             .parents(".accordion1")
//             .find(".accordion-item.active1")
//             .not($(this).parents(".accordion-item1"))
//             .removeClass("active");
//         $(this).parents(".accordion-item1").toggleClass("active");
//         $(this)
//             .parents(".accordion-item1")
//             .find(".accordion-content1")
//             .slideToggle(00);

//         // try {
//         //     bLazy.revalidate();
//         // }catch (e) {

//         // }
//         $(this)
//             .parents(".accordion1")
//             .find(".accordion-item1:not(.active)")
//             .find(".accordion-content1")
//             .slideUp(000);
//     });
// }
// function expand() {
//     $(".servicess").click(function () {
//         $(".servicess").find(".informations").removeClass("expand");
//         $(this).find(".informations").toggleClass("expand");

//         // $(this).find(".informations").addClass("expand");
//     });
// }
// function anime() {
//     $(".servicess").click(function () {
//         $(".servicess").find(".image").removeClass("anime");
//         $(this).find(".image").toggleClass("anime");

//         $(".servicess").each(function () {
//             $(".servicess").find(".image").css("width", "130px");
//             if ($(this).find(".image").hasClass("anime")) {
//                 sleep(500).then(() => {
//                     $(this).find(".image").css("width", "250px");
//                 });
//             }
//         });

//         // if ($(".servicess").find(".image").hasClass("anime")) {
//         // } else {
//         // }
//     });
// }

function animateServices() {
    $(".all-services.clickable .servicess").click(function () {
        $(this)
            .parents(".all-services")
            .find(".expanded")
            .removeClass("expanded");
        $(this).toggleClass("expanded");
    });
}



