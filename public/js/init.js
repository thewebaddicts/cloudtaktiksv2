var version = document.currentScript.getAttribute("attr-cache-version");

function loadCss(url) {
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    document.getElementsByTagName("head")[0].appendChild(link);
}

loadCss("/css/app.css?v=" + version);

loadCss("https://use.fontawesome.com/releases/v5.7.1/css/all.css");
loadCss("/js/owl.carousel/dist/assets/owl.carousel.min.css");
loadCss("/js/jquery.fancybox/dist/jquery.fancybox.min.css");
loadCss("/js/jquery.aos/dist/aos.css");
loadCss(
    "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
);

requirejs.config({
    waitSeconds: 200,
    paths: {
        functions: "/js/functions.js?v=" + version,
        jquery: "/js/jquery/dist/jquery.min.js?v=" + version,
        owl: "/js/owl.carousel/dist/owl.carousel.min.js?v=" + version,
        fancybox:
            "/js/jquery.fancybox/dist/jquery.fancybox.min.js?v=" + version,
        aos: "/js/jquery.aos/dist/aos.js?v=" + version,
        swiper: "https://unpkg.com/swiper/swiper-bundle.min.js",
    },
    shim: {
        functions: {
            deps: ["jquery", "fancybox"],
        },
        owl: {
            deps: ["jquery"],
        },
        aos: {
            deps: ["jquery"],
        },
        blazy: {
            deps: ["jquery"],
        },
        fancybox: {
            deps: ["jquery"],
        },
    },
});

//Define dependencies and pass a callback when dependencies have been loaded
require(["jquery"], function ($) {
    jQuery.event.special.touchstart = {
        setup: function (_, ns, handle) {
            this.addEventListener("touchstart", handle, { passive: true });
        },
    };
    var is_safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if (!is_safari) {
        $(window).on("beforeunload", function () {
            ShowLoader();
        });
    }
});

require(["functions"], function () {
    window.addEventListener("scroll", function () {
        InitializeMenuScroll();
    });

    try {
        InitializeMenuScroll();
    } catch (e) {
        console.log(e);
    }

    try {
        InitializeMenuMobile();
    } catch (e) {
        console.log(e);
    }

    try {
        console.log("before");
        FooterFunctions();
        console.log("after");
    } catch (e) {
        console.log(e);
    }

    try {
        FooterFunctions1();
    } catch (e) {
        console.log(e);
    }
    try {
        scrollup();
    } catch (e) {
        console.log(e);
    }
    try {
        thescrollingli();
    } catch (e) {
        console.log(e);
    }
    try {
        play();
    } catch (e) {
        console.log(e);
    }
    try {
        theli();
    } catch (e) {
        console.log(e);
    }
    try {
        NotificationFunction();
    } catch (e) {
        console.log(e);
    }
    try {
        hw2();
    } catch (e) {
        console.log(e);
    }
    try {
        hw();
    } catch (e) {
        console.log(e);
    }
    try {
        animations();
    } catch (e) {
        console.log(e);
    }
    try {
        thescrollingbutton();
    } catch (e) {
        console.log(e);
    }
    try {
        search1();
    } catch (e) {
        console.log(e);
    }
    try {
        fadein();
    } catch (e) {
        console.log(e);
    }
    try {
        searchforul();
    } catch {
        console.log(e);
    }

    try {
        videos1();
    } catch (e) {
        console.log(e);
    }
    try {
        clickthisa();
    } catch (e) {
        console.log(e);
    }
    try {
        times();
    } catch (e) {
        console.log(e);
    }
    try {
        searchforul2();
    } catch (e) {
        console.log(e);
    }
    try {
        regex($url, $matches);
    } catch (e) {
        console.log(e);
    }
    try {
        reveal();
    } catch (e) {
        console.log(e);
    }
    try {
        pop();
    } catch (e) {
        console.log(e);
    }

    try {
        searchProducts();
    } catch (e) {
        console.log(e);
    }
    try {
        AccordionInitiate()();
    } catch (e) {
        console.log(e);
    }
    try {
        AccordionInitiatee()();
    } catch (e) {
        console.log(e);
    }
    try {
        clicked()();
    } catch (e) {
        console.log(e);
    }
    // try {
    //     anime()();
    // } catch (e) {
    //     console.log(e);
    // }

    // minustoplus();
    // plustominus();
    // AccordionInitiate();
    // changeColor();
    // parallax();
    // parallax2();
    // minus();
    // scroll_style();
    // checkOffset();
    // AccordionInitiatee
});

require(["aos"], function (AOS) {
    AOS.init({ easing: "ease-in-out-sine", duration: 600, once: true });
});

require(["owl"], function (owlCarousel) {
    $(".carousel").each(function (index, elem) {
        var owl = $(elem);
        owl.owlCarousel({
            items: parseInt($(elem).attr("data-carousel-items")),
            nav: $(elem).attr("data-carousel-nav") === "true" ? true : false,
            dots: $(elem).attr("data-carousel-dots") === "true" ? true : false,
            autoplay:
                $(elem).attr("data-carousel-autoplay") === "true"
                    ? true
                    : false,
            slideTransition: "linear",
            autoplayHoverPause: false,

            autoWidth:
                $(elem).attr("data-carousel-autowidth") === "true"
                    ? true
                    : false,
            rtl: false,
            autoHeight: true,
            height: 500,
            // autoplayTimeout: 500,
            margin: 50,
            // animateOut: $(elem).attr("data-carousel-animate"),
            loop: $(elem).attr("data-carousel-loop") === "true" ? true : false,
            navText: [
                "<i class='fas fa-chevron-left '></i>",
                "<i class='fas fa-chevron-right'></i>",
            ],
        });
        $(".owl-dot").click(function () {
            owl.trigger("to.owl.carousel", [$(this).index(), 300]);
        });
        // owl.on("changed.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
        // owl.on("initialized.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
        // bLazy.revalidate();
    });

    $(".customized-carousel").each(function (index, elem) {
        // owl2.on('click', '.owl-next', function () {
        //     owl2.trigger('next.owl.carousel')
        // });
        // owl2.on('click', '.owl-prev', function () {
        //     owl2.trigger('prev.owl.carousel')
        // });
        // owl.on("changed.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
        // owl.on("initialized.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
        // bLazy.revalidate();
    });

    try {
        var owl2 = $(".customized-carousel");
        owl2.owlCarousel({
            items: parseInt(owl2.attr("data-carousel-items")),
            nav: owl2.attr("data-carousel-nav") === "true" ? true : false,
            dots: owl2.attr("data-carousel-dots") === "true" ? true : false,
            autoplay:
                owl2.attr("data-carousel-autoplay") === "true" ? true : false,
            slideTransition: "linear",
            autoplayHoverPause: false,
            autoWidth:
                owl2.attr("data-carousel-autowidth") === "true" ? true : false,
            rtl: false,
            autoHeight: true,
            height: 500,

            // margin:50,
            // animateOut: $(elem).attr("data-carousel-animate"),
            loop: owl2.attr("data-carousel-loop") === "true" ? true : false,
            dotsContainer: ".custom-owl-dots",
            navText: [
                "<i class='fas fa-chevron-left '></i>",
                "<i class='fas fa-chevron-right'></i>",
            ],
        });

        var owl3 = $(".customized-inner-carousel");
        owl3.owlCarousel({
            items: parseInt(owl3.attr("data-carousel-items")),
            nav: owl3.attr("data-carousel-nav") === "true" ? true : false,
            dots: owl3.attr("data-carousel-dots") === "true" ? true : false,
            autoplay:
                owl3.attr("data-carousel-autoplay") === "true" ? true : false,
            slideTransition: "linear",
            autoplayHoverPause: false,
            autoWidth:
                owl3.attr("data-carousel-autowidth") === "true" ? true : false,
            rtl: false,
            autoHeight: true,
            height: 500,
            // rewind: true,
            // margin:50,
            // animateOut: $(elem).attr("data-carousel-animate"),
            loop: owl3.attr("data-carousel-loop") === "true" ? true : false,
            dotsContainer: ".custom-owl-dots",
            navText: [
                "<i class='fas fa-chevron-left '></i>",
                "<i class='fas fa-chevron-right'></i>",
            ],
        });

        owl3.on("click", ".owl-next", function () {
            owl2.trigger("next.owl.carousel");
        });
        owl3.on("click", ".owl-prev", function () {
            owl2.trigger("prev.owl.carousel");
        });

        owl3.on("changed.owl.carousel", function (event) {
            var currentItem = event.item.index;
            owl2.trigger("to.owl.carousel", [currentItem, 300]);
        });
        owl2.on("changed.owl.carousel", function (event) {
            var currentItem = event.item.index;
            owl3.trigger("to.owl.carousel", [currentItem, 300]);
        });
    } catch (e) {
        console.log(e);
    }

    if ($(".single-carousel").length > 0) {
        var owl = $(".single-carousel");
        owl.owlCarousel({
            items: 1,
            nav: false,
            dots: true,
            //dotsContainer: ".owl-dots-custom",
            // autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: false,
            rtl: false,
            loop: true,
        });
        // owl.on("changed.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
        // owl.on("initialized.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
    }

    if ($(".multi-carousel").length > 0) {
        var owl2 = $(".multi-carousel");
        owl2.owlCarousel({
            autoWidth: true,
            autoplay: true,
            rtl: true,
            slidesToShow: 1,
            draggable: true,
            center: true,
            // loop: true,
            nav: true,
            items: 7,
            margin: 20,
            // padding: 100,
            dots: false,
        });
        owl2.on("changed.owl.carousel", function (event) {
            bLazy.revalidate();
        });
        owl2.on("initialized.owl.carousel", function (event) {
            bLazy.revalidate();
        });
    }
});

require(["fancybox"], function () {
    $.fancyConfirm = function (opts) {
        opts = $.extend(
            true,
            {
                title: "Are you sure?",
                message: "",
                okButton: "OK",
                noButton: "Cancel",
                callback: $.noop,
            },
            opts || {}
        );

        $.fancybox.open({
            type: "html",
            autoDimensions: true,
            src:
                '<div class="fc-content">' +
                "<h3>" +
                opts.title +
                "</h3>" +
                "<p>" +
                opts.message +
                "</p>" +
                '<div class="text-right confirmation">' +
                '<a data-value="0" data-fancybox-close>' +
                opts.noButton +
                "</a>" +
                '<button data-value="1" data-fancybox-close class="rounded">' +
                opts.okButton +
                "</button>" +
                "</div>" +
                "</div>",
            opts: {
                animationDuration: 350,
                animationEffect: "material",
                modal: true,
                baseTpl:
                    '<div class="fancybox-container fc-container" role="dialog" tabindex="-1">' +
                    '<div class="fancybox-bg"></div>' +
                    '<div class="fancybox-inner">' +
                    '<div class="fancybox-stage"></div>' +
                    "</div>" +
                    "</div>",
                afterClose: function (instance, current, e) {
                    var button = e ? e.target || e.currentTarget : null;
                    var value = button ? $(button).data("value") : 0;

                    opts.callback(value);
                },
            },
        });
    };

    try {
        NotificationFunction();
    } catch (e) {}
});
