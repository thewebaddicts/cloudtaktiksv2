<?php
use App\Http\Controllers\HomePageController;
use App\Http\Controllers\quoterequestController;
use App\Http\Controllers\AboutUsController;
use App\Http\Controllers\servicesController;
use App\Http\Controllers\cloudofferingController;
use App\Http\Controllers\citrixproductController;
use App\Http\Controllers\productproductController;
use App\Http\Controllers\customersController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\LocationsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// })->name('home');
Route::get('/', [HomePageController::class, 'render'])->name('home');

Route::get('/about', [AboutUsController::class, 'first'])->name('about-us');

Route::post('/quoterequest' , [quoterequestController::class, 'request'])->name('quote');

Route::get('/cloudoffering', [cloudofferingController::class, 'second'])->name('offerings');

Route::get('/citrix', [citrixproductController::class, 'third'])->name('product1');

Route::get('/products', [productproductController::class, 'fourth'])->name('product2');

Route::get('/services', [servicesController::class, 'fifth'])->name('service');

Route::get('/services/{id}', [servicesController::class, 'display_services'])->name('service1');

// Route::get('/services/{id}', [HomePageController::class, 'display_services'])->name('service1');

Route::get('/customers', [customersController::class, 'sixth'])->name('customers');

Route::post('/contact' , [ContactUsController::class, 'contact'])->name('contact');


Route::get('/contact' , [LocationsController::class, 'seventh'])->name('contact');

Route::get('/quoterequest', function () {
    return view('quoterequest');
})->name('quote');

// Route::get('/about', function () {
//     return view('about');
// })->name('about-us');

// Route::get('/contact', function () {
//     return view('contact');
// })->name('contact');

// Route::get('/cloudoffering', function () {
//     return view('cloudoffering');
// })->name('offerings');

// Route::get('/citrix', function () {
//     return view('citrix');
// })->name('product1');

// Route::get('/products', function () {
//     return view('products');
// })->name('product2');

// Route::get('/services', function () {
//     return view('services');
// })->name('service');

// Route::get('/customers', function () {
//     return view('customers');
// })->name('customers');

